import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Class for a chat server that that accepts connections from clients and
 * assigns them to new handler objects.
 *
 * @author Marcel Spitzer
 * @date: 7/24/15
 *
 * @see <a href="http://www.javaworld.com/article/2076864/java-concurrency/building-an-internet-chat-system.html">http://www.javaworld.com</a>
 */
public class ChatServer
{
    /**
     * Runs a server socket to accept new clients.
     *
     * @param port port number the server listens to
     * @throws IOException
     */
    public ChatServer( final int port ) throws IOException
    {
        // create new server socket
        final ServerSocket server = new ServerSocket( port );

        // run infinite loop for accepting clients
        while ( true )
        {
            // accept new client
            final Socket client = server.accept();
            System.out.println("Accepted from " + client.getInetAddress());

            // create new chat handler for accepted client
            final ChatHandler chatHandler = new ChatHandler(client);
            chatHandler.start();
        }
    }

    /**
     * Starts a chat server.
     *
     * @param args parameters with specified port
     * @throws IOException
     */
    public static void main( String args[] ) throws IOException
    {
        // check for correct arguments
        if ( args.length != 1 )
            throw new RuntimeException("Syntax: ChatServer <port>");

        // start chat server
        new ChatServer( Integer.parseInt(args[0]));
    }
}
