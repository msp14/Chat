import java.awt.*;
import java.io.*;
import java.net.Socket;

/**
 * Class for a simple chat client.
 *
 * @author Marcel Spitzer
 * @date 7/23/15
 *
 * @see <a href="http://www.javaworld.com/article/2076864/java-concurrency/building-an-internet-chat-system.html">http://www.javaworld.com</a>
 */
public class ChatClient extends Frame implements Runnable
{
    /**
     * input stream to receive messages
     */
    private final DataInputStream m_inputStream;
    /**
     * output stream to send messages
     */
    private final DataOutputStream m_outputStream;
    /**
     * output are for chat messages
     */
    private final TextArea m_outputArea;
    /**
     * input area for chat messages
     */
    private final TextField m_inputArea;
    /**
     * listener for user interactions
     */
    private Thread m_listener;

    /**
     * Performs basic setup for communication streams, GUI and threads.
     *
     * @param title a title for the window
     * @param inputStream input stream for chat messages
     * @param outputStream output stream for chat messages
     *
     * @todo find alternatives to deprecated methods
     */
    public ChatClient( final String title, final InputStream inputStream, final OutputStream outputStream)
    {
        super(title);

        // create buffered data streams to provide high-level communication facilities
        this.m_inputStream = new DataInputStream( new BufferedInputStream( inputStream ) );
        this.m_outputStream = new DataOutputStream( new BufferedOutputStream( outputStream ) );

        // set up a simple user interface
        setLayout( new BorderLayout() );
        add("Center", m_outputArea = new TextArea());
        m_outputArea.setEditable(false);
        add("South", m_inputArea = new TextField());
        pack();
        show();
        m_inputArea.requestFocus();

        // start a thread that accepts messages from the server
        m_listener = new Thread(this);
        m_listener.start();
    }

    /**
     * Starts a thread that receives messages from the server.
     *
     * @todo find alternatives to deprecated methods
     */
    public void run()
    {
        try
        {
            // infinite loop for reading strings from the input stream
            while( true )
            {
                // when a string arrives, append it to the output region
                final String line = m_inputStream.readUTF();
                m_outputArea.appendText( line + "\n");
            }
        }
        // catch IOException if the connection to the server has been lost
        catch ( IOException e )
        {
            e.printStackTrace();
        }
        // perform cleanup if IOException get caught or the thread is forcibly stopped
        finally
        {
            // perform cleanup
            m_listener = null;
            m_inputArea.hide();
            validate();

            try
            {
                m_outputStream.close();
            }
            catch ( IOException e )
            {
                e.printStackTrace();
            }
        }
    }

    /**
     * Handles user interaction.
     *
     * @param event UI event
     * @return true if handle was successful
     */
    public boolean handleEvent( final Event event )
    {
        // user has hit the return key
        if ( ( event.target == m_inputArea ) && ( event.id == Event.ACTION_EVENT ) )
        {
            try
            {
                // write message to the output stream and do flush
                m_outputStream.writeUTF( (String) event.arg );
                m_outputStream.flush();
            }
            // catch exception if connection has failed
            catch ( IOException e )
            {
                e.printStackTrace();
                m_listener.stop();
            }

            m_inputArea.setText("");
            return true;
        }
        // user attempted to close the window
        else if ( ( event.target == this ) && ( event.id == Event.WINDOW_DESTROY ) )
        {
            // stop the listener
            if ( m_listener != null )
                m_listener.stop();

            // hide the frame
            hide();
            return true;
        }

        return super.handleEvent(event);
    }

    /**
     * Performs the initial network connection and starts the chat client.
     *
     * @param args
     * @throws IOException
     */
    public static void main( String args[] ) throws IOException
    {
        // check for illegal arguments
        if ( args.length != 2 )
            throw new RuntimeException("Syntax: ChatClient <host> <port>");

        // create socket for communication and start chat client
        final Socket socket = new Socket(args[0], Integer.parseInt(args[1]));
        new ChatClient("Chat " + args[0] + ":" + args[1],
                socket.getInputStream(), socket.getOutputStream() );
    }
}
