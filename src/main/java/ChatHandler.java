import java.io.*;
import java.net.Socket;
import java.util.Enumeration;
import java.util.Vector;

/**
 * Class for a chat handler that will broadcast everything it reads
 * from any client. It is hardwired to read and write strings in UTF format
 * with data streams.
 *
 * @author Marcel Spitzer
 * @date: 7/24/15
 *
 * @see <a href="http://www.javaworld.com/article/2076864/java-concurrency/building-an-internet-chat-system.html">http://www.javaworld.com</a>
 */
public class ChatHandler extends Thread
{
    /**
     * the attached socket
     */
    private final Socket m_socket;
    /**
     * the input stream for chat communication
     */
    private final DataInputStream m_inputStream;
    /**
     * the output stream for chat communication
     */
    private final DataOutputStream m_outputStream;
    /**
     * keeps a list of all the current handlers
     */
    private final static Vector m_handlers = new Vector();

    /**
     * The constructor receives a reference to the client's socket and
     * opens an input and an output stream for chat communication.
     *
     * @param socket the attached socket
     * @throws IOException
     */
    public ChatHandler( final Socket socket ) throws IOException
    {
        m_socket = socket;
        m_inputStream = new DataInputStream( new BufferedInputStream( m_socket.getInputStream() ) );
        m_outputStream = new DataOutputStream( new BufferedOutputStream( m_socket.getOutputStream() ) );
    }

    /**
     * Performs client processing.
     */
    public void run()
    {
        try
        {
            // add current thread to handler list
            m_handlers.addElement( this );

            // receive and broadcast messages from the connected client
            while ( true )
            {
                final String message = m_inputStream.readUTF();
                broadcast(message);
            }
        }
        // catch exception if connection gets lost
        catch (IOException e)
        {
            e.printStackTrace();
        }
        // remove current thread from handler list and close the socket
        finally
        {
            m_handlers.removeElement(this);
            try
            {
                m_socket.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    /**
     * Broadcasts a message to all connected clients.
     *
     * @param message message to broadcast
     *
     * @todo find alternatives to deprecated methods
     */
    private final static void broadcast( final String message )
    {
        // synchronize list of handlers, because we do not want people joining
        // or leaving while we are looping. This prevents to broadcast a message
        // to clients who no longer exist.
        synchronized ( m_handlers )
        {
            // get list of currently connected clients to iterate through
            final Enumeration elements = m_handlers.elements();

            while ( elements.hasMoreElements() )
            {
                final ChatHandler currentHandler = (ChatHandler) elements.nextElement();

                try
                {
                    // synchronize output stream to prevent other threads from writing
                    // to the stream at the same time
                    synchronized ( currentHandler.m_outputStream )
                    {
                        currentHandler.m_outputStream.writeUTF( message );
                    }

                    currentHandler.m_outputStream.flush();
                }
                // if an error occurs while writing to a client, this client will get stopped
                catch (IOException e)
                {
                    currentHandler.stop();
                }
            }
        }
    }
}
